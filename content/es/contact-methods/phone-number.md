---
layout: page
title: Phone number
author: mfc
language: es
summary: Contact methods
date: 2018-09
permalink: /es/contact-methods/phone-number.md
parent: /es/
published: true
---

Mobile and landline phone communications are not encrypted to your recipients, so the content of your conversation and information about who you are calling is accessible by governments, law enforcement agencies, or other parties with the necessary technical equipment. 

---
layout: page
title: Email
author: mfc
language: es
summary: Contact methods
date: 2018-09
permalink: /es/contact-methods/email.md
parent: /es/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.
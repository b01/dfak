---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: Monday-Friday, 24/5, UTC-4
response_time: 6 hours
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect is a free website security service defending civil society and
human rights groups from digital attack.

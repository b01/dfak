---
layout: sidebar.pug
title: "Quelqu'un que je connais a été arrêté"
author: Peter Steudtner, Shakeeb Al-Jabri
language: fr
summary: "Un ami, un collègue ou un membre de la famille a été détenu par les forces de l'ordre. Vous souhaitez limiter l'impact de leur arrestation sur eux et sur toute autre personne qui pourrait être impliquée dans leur cas."
date: 2019-03-13
permalink: /fr/arrested/
parent: Home
sidebar: >
  <h3>Pour en savoir plus sur ce qu'il faut faire si quelqu'un que vous connaissez a été arrêté :</h3>

  <ul>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Obtenir du soutien pour faire campagne au nom de la personne détenue</a></li>
  </ul>
---

# Quelqu'un que je connais a été arrêté

Les arrestations de défenseurs des droits humains, de journalistes et de militants les mettent en grand danger, eux et ceux avec qui ils travaillent et vivent.

Ce guide concerne en particulier les pays où les droits de l'homme et les garanties d'une procédure régulière font défaut, où les autorités peuvent contourner les procédures judiciaires, les détentions ou les arrestations représentent alors un risque accru pour les victimes, leurs collègues et leurs proches.

Dans ce guide, nous visons à réduire le danger auquel les détenus sont exposés et à limiter leurs accès aux données sensibles qui peuvent incriminer d'autres victimes et leurs collègues, ou qui peuvent être utilisées pour compromettre d'autres opérations.

Prendre soin du détenu ainsi que des impacts numériques de cette détention peut s'avérer épuisant et difficile. Essayez de trouver d'autres personnes pour vous soutenir et coordonner vos actions avec la communauté concernée.

Il est également important que vous preniez soin de vous et des autres personnes touchées par cette arrestation, en :

- [prenant soin vos besoins et de votre bien-être psychosocial](../self-care),
- [considérant l'aspect juridique de votre travail de soutien](https://www.rarenet.org/resources/)
- [en obtenant du soutien pour faire campagne au nom de la personne détenue](https://www.newtactics.org/search/solr/arrest)

Si vous vous sentez techniquement ou émotionnellement dépassé ou (pour quelque raison que ce soit) dans l'impossibilité de suivre les étapes décrites ci-dessous, veuillez demander de l'aide et des conseils aux [organismes énumérés ici](../support) qui offrent ce type de soutien parmi leurs services.


## Élaborer un plan

Avant d'agir en suivant les différentes sections que nous décrivons ci-dessous, veuillez suivre les étapes suivantes :

- Essayez de lire le guide en entier et d'avoir une vue d'ensemble de tous les domaines importants avant d'agir sur des aspects particuliers. En effet, les différentes sections mettent en évidence différents scénarios de menace qui pourraient se chevaucher, vous devrez donc construire votre propre séquence d'actions.
- Prenez le temps, avec des amis ou une équipe, d'effectuer les différentes évaluations des risques qui sont nécessaires dans les différentes sections.

<a name="harm-reduction"></a>
## Préparez-vous avant d'agir

Avez-vous des raisons de croire que cette arrestation ou cette détention peut avoir des répercussions sur les membres de la famille, les amis ou les collègues du détenu, y compris vous-même ?

Ce guide vous aidera étape par étape à trouver des solutions susceptibles de réduire l'exposition du détenu et de toute personne potentiellement impliquée avec lui, dans le cas par exemple où des informations confidentielles sur des contacts se trouveraient sur des appareils du détenu, sur ses comptes de médias sociaux ou dans sa boîte aux lettres.

**Conseils pour une réponse coordonnée** :

Dans toutes les situations où une personne a été détenue, la première chose à noter est que souvent, lorsque de tels incidents se produisent, plusieurs amis ou collègues réagissent en même temps, ce qui entraîne des efforts redondants ou contradictoires. Il est donc important de se rappeler que la coordination et l'action concertée, aux niveaux local et international, sont nécessaires à la fois pour soutenir le détenu et pour prendre soin de tous les autres membres de son réseau de soutien, sa famille et ses amis.

- Mettez en place une équipe de crise qui coordonnera toutes les activités de soutien, de soins, de campagne, etc.
- Impliquez autant que possible les membres de la famille, les partenaires, etc. (en respectant leurs limites si, par exemple, ils sont débordés).
- Établissez des objectifs clairs pour votre campagne de soutien (et passez-les en revue fréquemment) : par exemple, vous pourriez vouloir obtenir la libération du détenu, assurer son bien-être, ou protéger sa famille et ses partisans et assurer leur bien-être comme objectifs les plus immédiats.
- Entendez-vous sur les canaux de communication sécurisés et la fréquence de leur utilisation, ainsi que sur les limites (p. ex. aucune communication entre 22h et 8h, sauf en cas de nouvelles urgences ou de nouvelles de dernière heure).
- Répartissez les tâches entre les membres de l'équipe et faites appel à des tiers pour obtenir leur soutien (analyse, plaidoyer, communication médias, documentation, etc.).
- Demandez un soutien supplémentaire à l'extérieur de l'"équipe de crise" pour répondre aux besoins de base (par exemple, repas réguliers, etc.).


**Précautions de sécurité numérique**

Si vous avez des raisons de craindre des répercussions pour vous-même ou pour d'autres sympathisants, avant de vous attaquer à toute urgence numérique liée à la détention de votre ami, il est également crucial de prendre des mesures préventives au niveau de la sécurité numérique pour vous protéger et protéger les autres du danger immédiat.

- Mettez-vous d'accord sur les canaux de communication (sécurisés) que votre réseau de sympathisants utilisera pour coordonner les mesures de réduction des risques et communiquer au sujet du détenu et des répercussions futures liées à son arestation.
- Réduisez le volume des données sur vos appareils au minimum nécessaire et protégez les données sur vos appareils avec le [chiffrement](https://ssd.eff.org/fr/module/assurer-la-s%C3%A9curit%C3%A9-de-vos-donn%C3%A9es#1).
- Créez des [sauvegardes sécurisées et chiffrées](https://communitydocs.accessnow.org/182-Secure_Backup.html) de toutes vos données et conservez-les dans un endroit impossible à trouver pendant des recherches éventuelles ou des détentions ultérieures.
- Partagez les mots de passe des appareils, des comptes en ligne, etc. avec une personne de confiance qui n'est pas en danger immédiat.
- Mettez-vous d'accord sur les mesures à prendre (comme la suspension de compte, l'effacement à distance, etc.) en guise de première réaction à votre éventuelle détention.

Ce guide vous guidera à travers une série d'étapes pour vous aider à trouver des solutions qui peuvent aider à réduire l'exposition du détenu et de toute personne qui est impliquée avec lui, par exemple parce que des informations sur ses contacts peuvent se trouver sur les appareils du détenu, les comptes des médias sociaux ou dans sa boîte aux lettres.


## Évaluation des risques
### Réduire les risques causés par nos propres actions

En général, vous devriez essayer de baser vos actions sur cette question :

- Quels sont les impacts des actions spécifiques et combinées sur les détenus, mais aussi sur leurs communautés, leurs collègues militants, leurs amis, leur famille, etc., y compris vous-même ?

Chacune des sections suivantes décrit les aspects particuliers de cette évaluation des risques.

Les considérations de base sont :

- Avant d'effacer des comptes, des données, des fils de discussion de médias sociaux, etc., assurez-vous d'avoir documenté le contenu et sauvegardé les informations que vous supprimez, surtout si vous devez restaurer ce contenu ou ces informations, ou si vous en avez besoin comme preuve ultérieure.
- Si vous supprimez ou effacez des comptes ou des fichiers, sachez que :
    - Les autorités pourraient interpréter cela comme une destruction ou un retrait de preuves.
    - Cela pourrait rendre la situation du détenu plus difficile, s'il a donné accès à ces comptes ou dossiers et que les autorités ne peuvent les trouver, car le détenu apparaîtrait comme non crédible et des actions contre lui pourraient être déclenchées par cette suppression.
- Si vous informez les gens que leurs renseignements personnels sont stockés dans un dispositif ou un compte saisi par les autorités et que cette communication est interceptée, cela pourrait être utilisé comme preuve supplémentaire du lien avec le détenu.
- Les changements dans les procédures de communication (y compris la suppression de comptes, etc.) pourraient attirer l'attention des autorités.

### Informer nos contacts

En général, il est impossible de déterminer si les autorités qui gardent prisonnier la personne ont la capacité de cartographier le réseau de contacts du détenu et si elles l'ont fait ou non. Nous devons donc supposer le pire des cas, qu'ils l'ont fait ou qu'ils vont le faire.

Avant de commencer à informer les contacts du détenu, veuillez évaluer le risque de les informer :
    
- Avez-vous une copie de la liste des contacts du détenu ? Pouvez-vous vérifier qui est sur leur liste de contacts, à la fois dans leurs appareils, leurs comptes de courriel et leurs plateformes de réseautage social ? Dressez la liste des personnes-ressources possibles afin d'avoir une vue d'ensemble des personnes susceptibles d'être touchées.
- Existe-t-il un risque que le fait d'informer les contacts en question puisse les lier plus étroitement au détenu et que les autorités qui gardent prisonnier la personne s'en servent contre eux ?
- Faut-il informer tout le monde, ou seulement un certain nombre de personnes dans cette liste de contacts ?
- Qui informera quels contacts ? Qui est déjà en contact avec qui ? Quel est l'impact de cette décision ?
- Établissez le canal de communication le plus sûr, y compris des rencontres personnelles dans des espaces où il n'y a pas de vidéosurveillance, pour informer les contacts impliqués.

### Documenter pour conserver les preuves

Avant de supprimer du contenu de sites Web, de sites de médias sociaux, de fils de discussion, etc., vous voudrez peut-être vous assurer que vous les avez déjà documentés. En les documentant, vous pourrez par exemple capturer tout signe ou preuve de comptes piratés - comme du contenu additionnel ou des usurpations d'identité - ou du contenu dont vous avez besoin comme preuve légale.

Selon le site Web ou la plate-forme de médias sociaux où vous souhaitez documenter les flux ou les données en ligne, différentes approches peuvent être utilisées :
    
- Vous pouvez faire des captures d'écran des parties concernées (assurez-vous que les informations de dates et d'heures, URLs, etc. sont inclus dans les captures d'écran).
- Vous pouvez vérifier que les sites Web ou blogs pertinents sont indexés dans la [Wayback Machine](https://archive.org/web), ou télécharger les sites Web ou blogs sur votre machine locale.

*Rappelez-vous qu'il est important de conserver les informations que vous avez téléchargées dans un dispositif sécurisé et stocké dans un endroit sûr.

### Saisie d'appareils

Si des dispositifs de la personne détenue ont été confisqués pendant ou après l'arrestation, veuillez lire le guide [J'ai perdu mon appareil](../topics/lost-device), en particulier la section sur [l'effacement à distance](../topics/lost-device/questions/find-erase-device), qui contient des recommandations en cas de détention.

### Considérer les données et comptes en ligne

Si le détenu dispose d'informations sur ses appareils qui pourraient lui nuire ou nuire à d'autres personnes, il peut être judicieux d'essayer de limiter l'accès du détenu à ces informations.

Avant de le faire, comparez le risque posé par ces informations avec le risque que les forces de sécurité soient mécontentes du fait du manque d'accès à ces informations (ou qu'une action en justice soit engagée en raison de la destruction de preuves). Si le risque posé par ces informations est plus élevé, vous pouvez procéder à la suppression des données pertinentes et/ou à la clôture/suspension et au découplage des comptes en suivant les instructions ci-dessous.

#### Suspendre ou fermer des comptes en ligne

Si vous avez accès aux comptes que vous souhaitez clôturer, vous pouvez suivre les processus des différents comptes. Assurez-vous d'avoir une copie de sauvegarde ou une copie du contenu et des données supprimés ! Sachez qu'après la fermeture d'un compte, le contenu ne deviendra pas immédiatement inaccessible : dans le cas de Facebook, par exemple, cela peut prendre jusqu'à deux semaines pour que le contenu soit supprimé de tous les serveurs.

Si vous n'avez pas accès aux comptes du détenu ou si vous avez besoin de mesures plus urgentes concernant les comptes de médias sociaux, veuillez obtenir le soutien des organisations énumérées [ici](../support) qui offrent la sécurisation des comptes parmi leurs services.

#### Dissocier les comptes des appareils

Parfois, vous pouvez également vouloir dissocier les comptes des appareils, car ce lien peut donner accès à des données sensibles à toute personne qui contrôle cet appareil. Pour ce faire, vous pouvez suivre [ces instructions](../topics/lost-device/questions/accounts).

N'oubliez pas de dissocier les [comptes bancaires en ligne](#online_bank_accounts) des appareils.

#### Changer les mots de passe

Si vous décidez de ne pas fermer ou suspendre les comptes, il peut toujours être utile de changer leurs mots de passe, en suivant [ces instructions](../topics/lost-device/questions/passwords).

Envisagez également de permettre l'authentification à deux facteurs pour accroître la sécurité des comptes du détenu, en suivant [ces instructions](../topics/lost-device/questions/2fa).

Si les mêmes mots de passe ont été utilisés sur différents comptes, vous devez changer tous les mots de passe concernés sur ces comptes, car ils peuvent également être compromis.

**Autres conseils pour changer les mots de passe**

- Utilisez un gestionnaire de mots de passe (comme [KeepassXC](https://keepassxc.org)) pour sauvegarder les mots de passe modifiés en vue d'une utilisation ultérieure ou pour les remettre au détenu après sa libération.
- Assurez-vous d'informer le détenu, au plus tard après sa libération, de la modification de ses mots de passe et de lui en rendre la propriété.

#### Retirer l'appartenance à des groupes et le partage de dossiers

Si le détenu fait partie d'un groupe (par exemple, mais sans s'y limiter) Facebook, WhatsApp, Signal ou Wire, ou s'il peut accéder à des dossiers partagés en ligne, et que sa présence dans ces groupes donne aux personnes qui le détiennent accès à des informations privilégiées et potentiellement dangereuses, vous pouvez le retirer des groupes ou espaces partagés en ligne.

**Instructions sur la façon de supprimer l'adhésion à un groupe sur les différents services de messagerie instantanée :**

- [WhatsApp](https://faq.whatsapp.com/en/android/26000116/?category=5245251&lang=fr)
- Télégramme - La personne qui a créé le groupe peut supprimer des participants en sélectionnant "Group Info" et en glissant l'utilisateur qu'ils veulent supprimer vers la gauche.
- Wire
    - [instructions pour mobile](https://support.wire.com/hc/en-us/articles/203526410)
    - [instructions pour l'application de bureau](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- Signal - Vous ne pouvez pas supprimer des participants des groupes Signal - ce que vous pouvez faire est de créer un nouveau groupe excluant le compte de la personne arrêtée.

**Instructions sur la façon d'annuler le partage de dossiers sur différents services en ligne :**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- GDrive - Recherchez d'abord les fichiers en utilisant l'opérateur de recherche "to:" (`to:username@gmail.com`), puis sélectionnez tous les résultats et cliquez sur l'icône Share. Cliquez sur Avancé, supprimez l'adresse de la boîte de dialogue et enregistrez.
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/fr-fr/HT201081) 

### Supprimer des messages de fils de discussion

Dans certains cas, vous voudrez peut-être retirer du contenu des fils de discussion des médias sociaux du détenu ou d'autres flux renvoyant à son compte, parce qu'il pourrait être utilisé comme preuve contre lui ou pour créer de la confusion et des conflits au sein de la communauté du détenu ou pour le discréditer.

Certains services facilitent la suppression des messages des comptes et des fils de discussion. Les guides pour Twitter, Facebook et Instagram sont en lien ci-dessous. Veuillez vous assurer que vous avez documenté le contenu que vous souhaitez supprimer, au cas où vous en auriez encore besoin comme preuve en cas de falsification, etc.

- Pour Twitter, vous pouvez utiliser [Tweet Deleters](https://tweetdeleter.com/).
- Pour Facebook, vous pouvez suivre [ce guide](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), basé sur une application pour navigateur Chrome appelée [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Pour Instagram, vous pouvez suivre [ces instructions](https://help.instagram.com/997924900322403)(mais attention, les personnes ayant accès aux paramètres de votre compte pourront voir l'historique de la plupart de vos interactions même après avoir supprimé du contenu).

### Supprimer les informations associées nuisibles

S'il y a des informations en ligne où le détenu est nommé et qui pourraient avoir des conséquences négatives pour lui ou ses contacts, il est bon de les supprimer si cela ne lui cause pas de préjudice supplémentaire.

- Dressez une liste des espaces en ligne et des informations qui doivent être supprimées ou modifiées.
- Si vous avez identifié le contenu à supprimer ou à modifier, vous voudrez peut-être le sauvegarder avant de procéder à sa suppression ou à des demandes de retrait.
- Évaluer si le retrait du nom du détenu pourrait avoir une incidence négative sur sa situation (p. ex. le retrait de son nom de la liste du personnel d'une organisation pourrait protéger l'organisation, mais il pourrait aussi retirer la justification du détenu, par exemple qu'il travaillait pour cette organisation).
- Si vous avez accès aux sites Web ou comptes respectifs, modifiez ou supprimez le contenu et les informations sensibles.
- Si vous n'y avez pas accès, demandez aux personnes qui y ont accès de supprimer les informations sensibles.
- Vous trouverez des instructions pour supprimer du contenu sur les services Google [ici](https://support.google.com/webmasters/answer/6332384?hl=fr#get_info_off_web)
- Vérifiez si les sites Web contenant des informations ont été indexés par la Wayback Machine ou Google Cache. Si c'est le cas, ce contenu doit également être supprimé.

<a name="online_bank_accounts"></a>
### Comptes bancaires en ligne

Très souvent, les comptes bancaires sont gérés et accessibles en ligne, et la vérification via des appareils mobiles peut être nécessaire pour les transactions ou même uniquement pour accéder au compte en ligne. Si un détenu n'accède pas à son compte bancaire pendant une longue période, cela pourrait avoir des répercussions sur sa situation financière et sur sa capacité d'accéder à ce compte. Dans ces cas-là, assurez-vous de :

- Détacher les appareils saisis du ou des comptes bancaires du détenu.
- Obtenir l'autorisation et la procuration du détenu pour que vous puissiez gérer son compte bancaire en son nom au début du processus (en accord avec ses proches).

## Derniers conseils

- Assurez-vous de restituer toutes les données au détenu après sa remise en liberté.
- Lisez [ces conseils](../topics/lost-device/questions/device-returned) sur la façon de traiter les appareils saisis après leur restitution par les autorités.


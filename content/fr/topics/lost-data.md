---
layout: page
title: "J'ai perdu mes données"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: fr
summary: "Que faire en cas de perte de données ?"
date: 2019-03-12
permalink: /fr/topics/lost-data
parent: /fr/
---

# I Lost my Data

Les données numériques peuvent être très éphémères et instables, et il existe de nombreuses façons de les perdre. Les dommages physiques subis par vos appareils, la résiliation de vos comptes, la suppression erronée, les mises à jour logicielles et les pannes logicielles peuvent tous entraîner une perte de données. De plus, il se peut que vous ne connaissiez pas toujours le fonctionnement de votre système de sauvegarde ou que vous ayez simplement oublié vos informations d'identification ou la façon de trouver ou récupérer vos données.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour diagnostiquer comment vous pourriez avoir perdu des données et vous donnera des stratégies d'atténuation potentielles pour les récupérer.

Voici un questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.


## Workflow

### entry_point

> Dans cette section, nous nous concentrons principalement sur les données stockées dans des appareils. En ce qui concerne le contenu en ligne et les identifiants, nous vous dirigerons vers d'autres sections de la trousse de premiers soins numériques.
>
> Les appareils en question comprennent les ordinateurs, les appareils mobiles, les disques durs externes, les clés USB et les cartes SD.

Quel type de données avez-vous perdu ?

- [Contenu en ligne](../../../account-access-issues)
- [Identifiant de compte](../../../account-access-issues)
- [Contenu sur un appareil](#content_on_device)

### content_on_device

> Souvent, vos données "perdues" sont des données qui ont simplement été effacées - accidentellement ou intentionnellement. Dans votre ordinateur, vérifiez votre Corbeille. Sur les appareils Android, vous pouvez trouver les données perdues dans le répertoire LOST.DIR. Si les fichiers manquants ou perdus ne peuvent pas être localisés dans la Corbeille de votre système d'exploitation, il est possible qu'ils n'aient pas été supprimés, mais qu'ils se trouvent à un emplacement différent de celui que vous aviez prévu. Essayez la fonction de recherche dans votre système d'exploitation pour les localiser.
>
> Vérifiez également les fichiers et dossiers cachés, car les données que vous avez perdues peuvent s'y trouver. Voici comment vous pouvez le faire sur [Mac](https://www.techadvisor.fr/tutoriel/ordinateurs/mac-afficher-les-fichiers-caches-3659724/) et[Windows](https://support.microsoft.com/fr-fr/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). Pour Linux, allez dans votre répertoire personnel dans un gestionnaire de fichiers et tapez Ctrl + H. Cette option est également disponible sur Android et iOS.
>
> Essayez de rechercher le nom exact de votre fichier perdu. Si cela ne fonctionne pas, ou si vous n'êtes pas sûr du nom exact, vous pouvez essayer une recherche de caractères génériques (par exemple, si vous avez perdu un fichier docx, mais que vous n'êtes pas sûr du nom, vous pouvez rechercher `*.docx`) qui ne fera apparaître que les fichiers avec l'extension `.docx`. Trier par *Date Modifié* afin de localiser rapidement les fichiers les plus récents lors de l'utilisation de l'option de recherche avec un caractère générique `*` (wildcard).
>
> En plus de rechercher les données perdues sur votre appareil, demandez-vous si vous les avez envoyées par e-mail ou partagées avec quelqu'un (y compris vous-même) ou si vous les avez ajoutées à votre stockage dans le cloud à un moment donné. Si c'est le cas, vous pouvez y effectuer une recherche et peut être, être en mesure de récupérer les données ou au moins une version de celles-ci.
>
> Pour augmenter vos chances de récupérer des données, cessez immédiatement d'utiliser vos appareils. L'écriture continue sur le disque dur peut diminuer les chances de trouver/restaurer des données. Imaginez que vous essayez de récupérer un message écrit sur un bloc-notes - plus vous écrivez sur les pages du bloc-notes, moins vous avez de chances de trouver ce que vous cherchez.

Comment avez-vous perdu vos données ?

- [L'appareil où les données ont été stockées a subi un dommage physique](#tech_assistance_end)
- [Le périphérique où les données ont été stockées a été volé/perdu](#device_lost_theft_end)
- [Les données ont été supprimées](#where_is_data)
- [Les données ont disparu après une mise à jour du logiciel](#software_update)
- [Le système ou un outil logiciel a planté et les données ont disparu](#where_is_data)


### software_update

> Parfois, lorsque vous mettez à jour ou mettez à niveau un outil logiciel ou l'ensemble du système d'exploitation, des problèmes inattendus peuvent survenir, entraînant l'arrêt du fonctionnement du système ou du logiciel comme il se doit, ou un comportement défectueux, ce qui peut entrainer la corruption des données. Si vous avez remarqué une perte de données juste après une mise à jour du logiciel ou du système, il vaut la peine d'envisager de revenir à un état antérieur. Les reculs sont utiles parce qu'ils signifient que votre logiciel ou base de données peut être restauré à un état propre et cohérent même après des opérations erronées ou lorsque des pannes logicielles se sont produites.

> La méthode pour ramener un logiciel à une version précédente dépend de la façon dont il a été installé - dans certains cas, c'est possible facilement, dans d'autres cas, cela impliquera un travail plus ou moins conséquent. Vous devrez donc faire une recherche en ligne sur le retour aux anciennes versions du logiciel en question. Veuillez noter que le retour en arrière est également connu sous le nom de "downgrading", alors faites une recherche avec ce terme aussi. Pour les systèmes d'exploitation tels que Windows ou Mac, chacun a ses propres méthodes pour revenir à la version précédente.

> * Pour les systèmes Mac, visitez la base de connaissances [Mac Support Center](https://support.apple.com/fr-fr) et utilisez le mot-clé "downgrade" avec votre version MacOS pour en savoir plus et trouver les instructions.
> * Pour les systèmes Windows, la procédure varie selon que vous souhaitez annuler une mise à jour spécifique ou revenir en arrière sur l'ensemble du système. Dans les deux cas, vous trouverez les instructions dans le [Microsoft Support Center](https://support.microsoft.com/fr-fr), par exemple pour Windows 10 vous pouvez chercher la question "Comment supprimer une mise à jour installée" dans cette [page FAQ](https://support.microsoft.com/fr-fr/help/12373/windows-update-faq).

Le retour à une version précédente du logiciel a-t-elle été utile ?

- [Oui](#resolved_end)
- [Non](#where_is_data)


### where_is_data

> La sauvegarde régulière des données est une bonne pratique à recommander. Parfois, nous oublions que la sauvegarde automatique est activée, il vaut donc la peine de vérifier sur vos appareils si cette option est activée, et d'utiliser cette sauvegarde pour restaurer vos données. Si ce n'est pas le cas, il est recommandé de planifier les sauvegardes futures, et vous trouverez d'autres conseils sur la façon de configurer les sauvegardes dans les [derniers conseils de ce déroulé](#resolved_end).

Pour vérifier si vous avez une sauvegarde des données perdues, commencez à vous demander où les données perdues ont été stockées.

- [Dans un périphérique de stockage (disque dur externe, clés USB, cartes SD)](#storage_devices_end)
- [Dans un ordinateur](#computer)
- [Dans un appareil mobile](#mobile)


### computer

Quel type de système d'exploitation est installé sur votre ordinateur ?

- [MacOS](#macos_computer)
- [Windows](#windows_computer)
- [Gnu/Linux](#linux_computer)

### macos_computer

> Pour vérifier si une option de sauvegarde est activée sur votre périphérique MacOS et utiliser ainsi cette sauvegarde pour restaurer vos données, vérifiez vos options [iCloud](https://support.apple.com/fr-fr/HT208682) ou [Time Machine](https://support.apple.com/fr-fr/HT201250) pour voir si une sauvegarde est disponible.

> Un des endroits à consulter est la liste des éléments récemment utilisés, qui garde la trace des applications, des fichiers et des serveurs que vous avez utilisés au cours de vos dernières sessions sur l'ordinateur. Pour rechercher le fichier et le rouvrir, allez dans le menu Pomme dans le coin supérieur gauche, sélectionnez Éléments récents et parcourez la liste des fichiers. Plus de détails sont disponibles [ici](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-lost-files.html).

Avez-vous pu localiser vos données ou les restaurer ?

- [Oui](#resolved_end)
- [Non](#tech_assistance_end)


### windows_computer

> Pour vérifier si une sauvegarde est activée sur votre machine Windows, vous pouvez lire [ces instructions](https://support.microsoft.com/fr-fr/help/4027408/windows-10-backup-and-restore).
>
> Windows 10 inclut la fonction **Chronologie** (Timeline), qui a pour but d'améliorer votre productivité en enregistrant les fichiers que vous avez utilisés, les sites que vous avez consultés et les autres actions que vous avez effectuées sur votre ordinateur. Si vous ne vous souvenez plus où vous avez stocké un document, vous pouvez cliquer sur l'icône Chronologie dans la barre des tâches de Windows 10 pour voir un journal visuel organisé par date, et revenir à ce dont vous avez besoin en cliquant sur l'icône de prévisualisation approprié. Cela peut vous aider à localiser un fichier qui a été renommé. Si la Chronologie est activée, certaines de vos activités sur le PC - comme les fichiers que vous éditez dans Microsoft Office - peuvent également se synchroniser avec votre appareil mobile ou un autre ordinateur que vous utilisez, de sorte que vous pouvez avoir une sauvegarde de vos données perdues sur un autre appareil. Vous pouvez en savoir plus sur la Chronologie et comment l'utiliser, [ici](https://support.microsoft.com/fr-fr/help/4230676/windows-10-get-help-with-timeline).

Avez-vous pu localiser vos données ou les restaurer ?

- [Oui](#resolved_end)
- [Non](#windows_restore)


### windows_restore

> Dans certains cas, il existe des outils gratuits et open-source qui peuvent être utiles pour trouver le contenu manquant et le récupérer. Parfois, leur utilisation est limitée et la plupart des outils connus coûtent de l'argent.
>
> Par exemple [Recuva](http://www.ccleaner.com/recuva) est un logiciel libre connu pour la récupération de données. Nous vous recommandons de lui donner une chance et de voir comment ça se passe.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech_assistance_end)


### linux_computer

> Certaines des distributions Linux les plus populaires, comme Ubuntu, ont un outil de sauvegarde intégré, par exemple [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) sur Ubuntu. Si un outil de sauvegarde intégré est inclus dans votre système d'exploitation, vous avez peut-être été invité à activer les sauvegardes automatiques lorsque vous avez commencé à utiliser votre ordinateur. Effectuez une recherche dans votre distribution Linux pour voir si elle inclut un outil de sauvegarde intégré, et quelle est la procédure à suivre pour vérifier si il est activé et pour restaurer les données à partir des sauvegardes.
>
> Pour Déja Dup sur Ubuntu, vous pouvez consulter [cet article](https://doc.ubuntu-fr.org/deja-dup) pour vérifier si les sauvegardes automatiques sont activées sur votre machine, et sur [cette page](https://wiki.gnome.org/DejaDup/Help/Restore/Full) pour savoir comment restaurer vos données perdues depuis une sauvegarde existante.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech_assistance_end)


### mobile

Quel système d'exploitation fonctionne sur votre mobile ?

- [iOS](#ios_phone)
- [Android](#android_phone)


### ios_phone

> Vous avez peut-être activé une sauvegarde automatique avec iCloud ou iTunes. Lisez [ce guide](https://support.apple.com/kb/ph12521?locale=fr_FR) pour vérifier si vous avez des sauvegardes existantes et pour apprendre comment restaurer vos données.

Avez-vous trouvé votre sauvegarde et récupéré vos données ?

- [Oui](#resolved_end)
- [Non](#phone_which_data)

###  phone_which_data

Lequel des énoncés suivants s'applique aux données que vous avez perdues ?

- [Il s'agit de données générées par des applications, par exemple les contacts, les flux, etc.](#app_data_phone)
- [Il s'agit de données générées par l'utilisateur, par exemple photos, vidéos, audio, notes](#ugd_phone)

### app_data_phone

> Une application mobile est une application logicielle conçue pour fonctionner sur votre appareil mobile. Cependant, la plupart de ces applications sont également accessibles via un navigateur sur un ordinateur de bureau. Si vous avez subi une perte de données générées par les applications dans vos téléphones mobiles, essayez d'accéder à cette application dans votre navigateur d'ordinateur de bureau, en vous connectant à l'interface Web de l'application avec vos identifiants pour cette application. Il se peut que vous y trouviez les données perdues via l'interface du navigateur.

- [Oui](#resolved_end)
- [Non](#tech_assistance_end)

### ugd_phone

> Les données générées par l'utilisateur sont le type de données que vous créez ou générez via une application spécifique, et en cas de perte de données, vous pouvez vérifier si les paramètres de sauvegarde de cette application sont activés par défaut ou si elle permet de les restaurer. Par exemple, si vous utilisez WhatsApp sur votre mobile et que les conversations ont disparu ou que quelque chose de défectueux s'est produit, vous pouvez récupérer vos conversations si vous avez activé le paramètre de récupération de WhatsApp, ou si vous utilisez une application pour créer et conserver des notes contenant des informations sensibles ou personnelles, elle peut également avoir une option de sauvegarde active sans que vous le remarquiez parfois.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech_assistance_end)

### android_phone

> Google a un service intégré dans Android, appelé Android Backup Service. Par défaut, ce service sauvegarde plusieurs types de données et les associe au service Google approprié, auquel vous pouvez également accéder sur le Web. Vous pouvez voir vos paramètres de synchronisation en allant dans Paramètres > Comptes > Google, puis en sélectionnant votre adresse Gmail. Si vous avez perdu des données que vous synchronisiez avec votre compte Google, vous pourrez probablement les récupérer en vous connectant à votre compte Google via un navigateur Web.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#phone_which_data)


### tech_assistance_end

> Si votre perte de données a été causée par des dommages physiques tels que la chute de vos appareils sur le sol ou dans l'eau, suite à une panne d'électricité ou d'autres problèmes, la situation la plus probable est que vous devrez effectuer une récupération des données stockées dans les disques durs. Si vous ne savez pas comment effectuer ces opérations, il est plus simple de contacter une personne douée en informatique qui possède le matériel et l'équipement électronique qui pourrait vous aider. Cependant, en fonction de votre contexte et de la sensibilité des données que vous devez récupérer, nous vous déconseillons de contacter un service informatique, mais plutôt de vous adresser à des personnes douées en informatique que vous connaissez et en lesquelles vous avez confiance.


### device_lost_theft_end

> En cas de perte ou de vol d'appareils, assurez-vous de changer tous vos mots de passe dès que possible et de visiter notre [ressource spécifique sur ce qu'il faut faire si un appareil est perdu](../../../I lost my devices).
>
> Si vous êtes membre de la société civile et que vous avez besoin d'aide pour acquérir un nouvel appareil afin de remplacer celui que vous avez perdu, vous pouvez vous adresser aux organisations énumérées ci-dessous.

:[](organisations?services=equipment_replacement)


### storage_devices_end

> Pour récupérer les données que vous avez perdues, rappelez-vous que le timing est important. Par exemple, la récupération d'un fichier que vous avez supprimé accidentellement quelques heures ou la veille peut avoir un taux de réussite supérieur à celui d'un fichier perdu des mois auparavant.
>
> Envisagez d'utiliser un outil logiciel pour essayer de récupérer les données que vous avez perdues (à cause d'une suppression ou d'une corruption) tel que [Recuva pour Windows](https://www.ccleaner.com/recuva/download), et pour Gnu/Linux vous pouvez rechercher les différents logiciels disponibles pour votre distribution. Tenez compte du fait que ces outils ne fonctionnent pas toujours, car votre système d'exploitation peut avoir écrit de nouvelles données sur vos informations supprimées. Pour cette raison, vous devriez utiliser le moins possible votre ordinateur entre le moment où vous avez supprimé un fichier et celui où vous essayez de le restaurer avec un outil comme Recuva.
>
> Pour augmenter vos chances de récupérer des données, cessez immédiatement d'utiliser vos appareils. L'écriture continue sur le disque dur peut diminuer les chances de trouver et de restaurer les données.
>
> Si aucune des options ci-dessus n'a fonctionné pour vous, la situation la plus probable est que vous devrez effectuer une récupération des données stockées dans les disques durs. Si vous ne savez pas comment effectuer ces opérations, il est plus simple de contacter une personne douée en informatique qui possède le matériel et l'équipement électronique et qui pourrait vous aider. Cependant, en fonction de votre contexte et de la sensibilité des données que vous devez récupérer, nous vous déconseillons de contacter un service informatique, mais plutôt de vous adressez à des personnes douées en informatique que vous connaissez et en lesquelles vous avez confiance.


### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Sauvegardes - En plus des différents conseils ci-dessus, c'est toujours une bonne idée de vous assurer d'avoir des sauvegardes - beaucoup de sauvegardes différentes, que vous stockez ailleurs que vos données ! Selon votre contexte, optez pour le stockage de vos sauvegardes dans les services cloud et dans des périphériques physiques externes que vous gardez déconnectés de votre ordinateur lors de votre connexion à Internet.
- Pour les deux types de sauvegardes, vous devez protéger vos données par cryptage. Effectuez des sauvegardes régulières et incrémentales de vos données les plus importantes et vérifiez qu'elles sont réalisées avant d'effectuer des mises à jour logicielles ou OS.
- Mettez en place un système de dossiers structurés - Chaque personne a sa propre façon d'organiser ses données et informations importantes, il n'y a pas de règle unique pour tous. Néanmoins, il est important que vous envisagiez de mettre en place un système de dossiers adapté à vos besoins. En créant un système de dossiers cohérent, vous pouvez vous faciliter la vie en sachant mieux quels dossiers et fichiers doivent être sauvegardés fréquemment, par exemple, où se trouvent les informations importantes sur lesquelles vous travaillez, où vous devez conserver les données contenant des informations personnelles ou sensibles sur vous et vos collaborateurs, etc. Prenez le temps pour planifier le type de données que vous produisez ou gérez, et pensez à un système de dossiers qui peut rendre votre organisation plus cohérente.


#### resources

- [Security in a Box - Tactiques de sauvegarde](https://securityinabox.org/en/guide/backup/)
- [Page officielle sur les sauvegardes Mac](https://support.apple.com/fr-fr/mac-backup)
- [Comment sauvegarder régulièrement des données sous Windows 10](https://support.microsoft.com/fr-fr/help/4027408/windows-10-backup-and-restore)
- [Comment activer les sauvegardes régulières sur iPhone](https://support.apple.com/fr-fr/HT203977)
- [Comment activer les sauvegardes régulières sur Android](https://support.google.com/android/answer/2819582?hl=fr).


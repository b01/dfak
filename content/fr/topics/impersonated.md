---
layout: page
title: "Quelqu'un se fait passer pour moi en ligne"
author: Floriana Pagano, Alexandra Hache
language: fr
summary: "Quelqu'un s'est fait usurper son identité par le biais d'un compte de média social, d'une adresse e-mail, d'une clé PGP, d'un faux site Web ou d'une fausse application"
date: 2019-04-01
permalink: /fr/topics/impersonated
parent: /fr/
---

# Someone is Impersonating Me Online

Une menace à laquelle font face de nombreux militants, défenseurs des droits humains, ONG, médias indépendants et blogueurs réside dans l'usurpation de leur identité par des personnes qui créeront de faux profils, sites Web ou courriels en leur nom. Ces personnes peuvent ainsi créer des campagnes de diffamation, d'informations trompeuses, d'ingénierie sociale ou de vol d'identité afin de créer du bruit, des problèmes de confiance et des atteintes à la protection des données qui ont un impact sur la réputation des individus et des collectifs dont l'identité est usurpée. C'est un problème frustrant qui peut affecter à différents niveaux votre capacité à communiquer et à informer, et il peut avoir différentes causes selon l'endroit et la façon avec laquelle votre identité est usurpée.

Il est important de savoir qu'il existe de nombreuses façons de se faire passer pour quelqu'un (faux profils dans les médias sociaux, sites Web clonés, courriels frauduleux, publication non consensuelle d'images et de vidéos personnelles). Les stratégies peuvent aller de la soumission d'avis de retrait, à la preuve de la propriété originale, en passant par la revendication des droits d'auteur du site Web ou de l'information originale, ou l'avertissement de vos réseaux et contacts personnels par des communications publiques ou confidentielles. Diagnostiquer le problème et trouver des solutions possibles à l'usurpation d'identité peut s'avérer compliqué. Parfois, il sera presque impossible de pousser une petite société d'hébergement à faire tomber un site Web, et une action en justice peut s'avérer nécessaire. Il est recommandé d'établir des alertes et de surveiller Internet pour savoir si vous ou votre organisation avez été usurpé.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour diagnostiquer les moyens potentiels de se faire passer pour vous et les stratégies d'atténuation potentielles pour supprimer les comptes, sites Web et courriels qui se font passer pour vous ou votre organisation. 

Si votre identité a été usurpée, suivez ce questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.


## Workflow

### urgent_question

Craignez-vous pour votre intégrité physique ou votre bien-être ?

 - [Oui](#physical_sec_end)
 - [Non](#diagnostic_start1)

### diagnostic_start1

L'usurpation d'identité vous affecte-t-elle en tant qu'individu (quelqu'un utilise votre nom et prénom légal, ou le surnom sur lequel vous basez votre réputation) ou en tant qu'organisation/collectif ?

- [En tant que personne](#individual)
- [En tant qu'organisation](#organization)
 
### individual

Si vous êtes affecté en tant qu'individu, vous voudrez peut-être alerter vos contacts. Effectuez cette étape à l'aide d'un compte de messagerie, d'un profil ou d'un site Web qui est entièrement sous votre contrôle.

- Une fois que vous avez informé vos contacts que votre identité a été usurpée, passez à l'[étape suivante](#diagnostic_start2)


### organization

> Si vous êtes affecté en tant que groupe, vous voudrez peut-être faire une communication publique. Pour ce faire, utilisez un compte de messagerie, un profil ou un site Web qui est entièrement sous votre contrôle.

- Une fois que vous avez informé votre communauté que votre identité a été usurpée, passez à l'étape suivante (#diagnostic_start2).


### diagnostic_start2

Comment est-ce que quelqu'un se fait passer pour vous ?

 - [Un faux site Web se fait passer pour moi ou pour mon groupe](#fake_website)
 - [Par le biais d'un compte de réseau social](#social_network)
 - [Par le partage non consensuel de vidéos ou d'images] (#other_website)
 - [Par mon adresse e-mail ou une adresse similaire](#spoofed_email1)
 - [Par une clé PGP connectée à mon adresse email](#PGP)
 - [Par une fausse application qui imite mon application](#app1)


### social_network

Sur quelle plateforme de réseautage social quelqu'un se fait-il passer pour vous ?
On which social networking platform are you being impersonated?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)

###  facebook

> Suivez [ces instructions](https://www.facebook.com/help/174210519303259) pour demander la suppression du compte usurpateur de votre identité.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que ça a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)

### twitter

> Remplissez [ce formulaire](https://help.twitter.com/forms/impersonation) pour demander la suppression du compte usurpateur de votre identité.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)

### google

> Remplissez [ce formulaire](https://support.google.com/plus/troubleshooter/1715140) pour demander la suppression du compte usurpateur de votre identité.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)

### instagram


> Remplissez [ce formulaire](https://help.instagram.com/446663175382270) pour demander la suppression du compte usurpateur de votre identité.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)

### fake_website

> Vérifiez si ce site Web est connu comme étant malveillant en recherchant son URL dans les services en ligne suivants :
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

Le domaine est-il connu pour être malveillant ?

 - [Oui](#malicious_website)
 - [Non](#non-malicious_website)

### malicious_website

> Signalez l'URL à Google Safe Browsing en remplissant [ce formulaire](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Veuillez noter qu'il se peut qu'il faille un certain temps pour s'assurer que votre signalement soit pris en compte. En attendant, vous pouvez passer à l'étape suivante pour envoyer une demande de fermeture du site au fournisseur d'hébergement et au bureau d'enregistrement du nom de domaine (registrar), ou enregistrer cette page dans vos signets et revenir à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#non-malicious_website)


### non-malicious_website

> Vous pouvez essayer de signaler le site Web au fournisseur d'hébergement ou au bureau d'enregistrement de domaine, en demandant la fermeture du site.
>
> Si le site Web que vous voulez signaler utilise votre contenu, vous devrez peut-être prouver que vous êtes le propriétaire légitime du contenu original. Vous pouvez le prouver en présentant votre contrat original avec le bureau d'enregistrement du domaine et/ou le fournisseur d'hébergement, mais vous pouvez également effectuer une recherche sur la [Wayback Machine](https://archive.org/web/), en recherchant à la fois l'URL de votre site Web et le faux site. Si les sites y ont été indexés, vous y trouverez un historique qui peut permettre de montrer que votre site existait avant la publication du faux site.
>
> Pour envoyer une demande de fermeture, vous devrez également recueillir des informations sur le faux site Web :
>
> Allez sur [ce site](https://network-tools.com/nslookup/) et trouvez l'adresse IP (ou les adresses IP) du faux site en entrant son URL dans le formulaire de recherche.
> Notez l'adresse IP ou les adresses IP.
> Allez sur [ce site Web](https://whois.domaintools.com/) et cherchez à la fois le domaine et l'adresse IP du ou des faux sites Web.
> Enregistrer le nom et l'adresse e-mail abusive du fournisseur d'hébergement et du service d'enregistrement du domaine. S'il est inclus dans les résultats de votre recherche, inscrivez également le nom du propriétaire du site Web.
> Ecrivez au fournisseur d'hébergement et au bureau d'enregistrement du faux site web pour demander sa fermeture. Dans votre message, indiquez des informations sur l'adresse IP, l'URL et le propriétaire du site Web usurpateur de votre identité, ainsi que les raisons pour lesquelles il est abusif.
> Vous pouvez utiliser [ce modèle](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) pour écrire au fournisseur d'hébergement.
> Vous pouvez utiliser [ce modèle](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) pour écrire au registrar du domaine.
> 
> Veuillez noter qu'il peut prendre un certain temps avant de recevoir une réponse à vos demandes. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que cela a marché ?

- [Oui](#resolved_end)
- [Non](#web_protection_end)


### spoofed_email1

> Pour des raisons techniques sous-jacentes, il est assez difficile d'authentifier les e-mails. C'est aussi la raison pour laquelle il est très facile de créer de fausses adresses d'expéditeurs et des e-mails falsifiés.

Vous faites-vous usurper votre identité par le biais de votre adresse e-mail ou d'une adresse similaire, par exemple avec le même nom d'utilisateur, mais un domaine différent ?

- [Je me fais usurper mon identité par mon adresse e-mail](#spoofed_email2)
- [Je me fais usurper mon identité par le biais d'une adresse e-mail similaire](#similar_email)


### spoofed_email2

> La personne qui se fait passer pour vous a peut-être piraté votre compte de messagerie. Pour écarter cette possibilité, essayez de changer votre mot de passe.

Pouvez-vous changer votre mot de passe ?

- [Oui](#spoofed_email3)
- [Non](#hacked_account)

### hacked_account

Si vous ne pouvez pas changer votre mot de passe, votre compte de courriel est probablement compromis.

- Vous pouvez suivre [ce déroulé](../../../account-access-issues) pour résoudre ce problème.


### spoofed_email3

> L'usurpation d'identité consiste à envoyer des messages électroniques avec une adresse d'expéditeur falsifiée. Le message semble provenir d'une personne ou d'un endroit autre que la source réelle.
>
> L'usurpation d'adresse électronique est courante dans les campagnes d'hameçonnage et de pourriels (spams) parce que les gens sont plus susceptibles d'ouvrir un courriel lorsqu'ils pensent qu'il provient d'une source légitime.
>
> Si quelqu'un usurpe votre courriel, vous devriez informer vos contacts pour les avertir du danger de l'hameçonnage (faites-le à partir d'un compte de courriel, d'un profil ou d'un site Web qui est entièrement sous votre contrôle).
>
> Si vous pensez que l'usurpation d'identité visait l'hameçonnage ou d'autres intentions malveillantes, vous pouvez également lire la section [J'ai reçu des messages suspects](../../../suspicious_messages).

Est-ce que les courriels se sont arrêtés après que vous ayez changé le mot de passe de votre compte de courriel ?

- [Oui](#compromised_account)
- [Non](#secure_comms_end)


### compromised_account

> Votre compte a probablement été piraté par quelqu'un qui l'a utilisé pour envoyer des courriels pour se faire passer pour vous. Comme votre compte a été compromis, vous pouvez également lire la section [J'ai perdu l'accès à mes comptes](../../../account-access-issues/).

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#account_end)


### similar_email

> Si l'usurpateur utilise une adresse e-mail similaire à la vôtre mais avec un nom de domaine ou d'utilisateur différent, il est conseillé d'avertir vos contacts de cette tentative d'usurpation d'identité (à partir d'un compte mail, profil ou site web qui est entièrement sous votre contrôle).
>
> Vous pouvez également lire la section [J'ai reçu des messages suspects](../../../suspicious-messages), car cette usurpation d'identité pourrait viser l'hameçonnage.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#secure_comms_end)


### PGP

Pensez-vous que votre clé PGP privée a pu être compromise, par exemple parce que vous avez perdu le contrôle de l'appareil où elle était stockée ?

- [Oui](#PGP_compromised)
- [Non](#PGP_spoofed)

### PGP_compromised

Avez-vous toujours accès à votre clé privée ?

- [Oui](#access_to_PGP)
- [Non](#lost_PGP)

### access_to_PGP

> Révoquez votre clé.
> - [Instructions pour Enigmail](https://www.enigmail.net/documentation/Key_Management#Revoking_your_key_pair_Management#Revoking_your_key_pair)
> Créez une nouvelle paire de clés et faites-la signer par des personnes en qui vous avez confiance.
> En communiquant par un canal que vous contrôlez, informez vos contacts que vous avez révoqué votre clé et en avez généré une nouvelle.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure_comms_end)
- [Non](#resolved_end)


### lost_PGP

Avez-vous un certificat de révocation ?

- [Oui](#access_to_PGP)
- [Non](#no_revocation_cert)


### no_revocation_cert
> - Créez une nouvelle paire de clés et faites-la signer par des personnes en qui vous avez confiance.
> - Informez vos contacts par un canal que vous contrôlez qu'ils doivent utiliser votre nouvelle clé et cesser d'utiliser l'ancienne.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure_comms_end)
- [Non](#resolved_end)

### PGP_spoofed

Votre clé est-elle signée par des personnes de confiance ?

- [Oui](#signed_key)
- [Non](#non-signed_key)

### signed_key

> Informez vos contacts par l'intermédiaire d'un canal que vous contrôlez que quelqu'un essaie de se faire passer pour vous et dites-leur qu'ils peuvent reconnaître votre clé actuelle en se basant sur les signatures des contacts de confiance.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure_comms_end)
- [Non](#resolved_end)

### non-signed_key

> - Faites signer votre clé par des personnes en qui vous avez confiance.
> - Informez vos contacts par l'intermédiaire d'un canal que vous contrôlez que quelqu'un essaie de se faire passer pour vous et dites-leur qu'ils peuvent reconnaître votre clé réelle en se basant sur les signatures des contacts de confiance.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure_comms_end)
- [Non](#resolved_end)

### other_website

> Si votre identité a été usurpée sur un site Web, la première chose que vous devez faire est de comprendre où ce site Web est hébergé, qui le gère et qui a fourni le nom de domaine. Cette recherche vise à identifier la meilleure façon de demander le retrait du contenu malveillant.
>
> Avant de poursuivre votre investigation, si vous êtes citoyen de l'UE, vous pouvez demander à Google de supprimer ce site Web de ses résultats de recherche liés à votre nom.

Vous êtes citoyen de l'Union européenne ?

- [Oui](#EU_privacy_removal)
- [Non](#doxing_question)


### EU_privacy_removal


> Remplissez [ce formulaire](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=fr&rd=1) pour supprimer ce site Web des résultats de recherche Google liés à votre nom.
>
> Ce dont vous aurez besoin :
>
> - Une copie numérique d'un document d'identification (si vous soumettez cette demande au nom de quelqu'un d'autre, vous devrez fournir des documents d'identification pour cette personne).
> - L'URL(s) du contenu contenant les informations personnelles que vous souhaitez supprimer.
> - Pour chaque URL que vous fournissez, vous devrez expliquer :
>     1. le lien entre les renseignements personnels susmentionnés et la personne au nom de laquelle la demande est présentée
>     2. les raisons pour lesquelles vous croyez que les renseignements personnels devraient être supprimés
>
> Veuillez noter que si vous êtes connecté à votre compte Google, Google peut associer automatiquement votre signalement à ce compte.
>
> Après avoir soumis ce formulaire, vous devrez attendre une réponse de Google pour vérifier que les résultats ont été supprimés.

Souhaitez-vous déposer une demande de retrait pour supprimer du site Web le contenu usurpant votre identité  ?

- [Oui](#doxing_question)
- [Non, j'aimerais recevoir de l'aide](#account_end)

### doxing_question

L'usurpateur a-t-il publié des renseignements personnels, des vidéos intimes ou des images de vous ?

- [Oui](../../../harassed-online/questions/doxing_web)
- [Non](#fake_website)

### app1

> Si quelqu'un répand une copie malveillante de votre application ou d'un autre logiciel, c'est une bonne idée de faire une communication publique pour avertir les utilisateurs de ne télécharger que la version légitime.
>
> Vous devez également signaler l'application malveillante et demander son retrait.

Où la copie malveillante de votre application est-elle distribuée ?

- [Sur Github](#github)
- [Sur Gitlab.com](#gitlab)
- [Sur Google Play Store](#playstore)
- [Sur l'Apple App Store](#apple_store)
- [Sur un autre site web](#fake_website)

### github

> Si le logiciel malveillant est hébergé sur Github, lisez [ce guide](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) pour retirer le contenu qui viole le droit d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)

### gitlab

> Si le logiciel malveillant est hébergé sur Gitlab.com, lisez [ce guide](https://about.gitlab.com/handbook/dmca/) pour retirer le contenu qui viole le droit d'auteur.
>
> Cela peut prendre [un certain temps](https://about.gitlab.com/handbook/engineering/security/dmca-removal-requests.html) pour attendre une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)


### playstore

> Si l'application malveillante est hébergée sur Google Play Store, suivez [ces instructions](https://support.google.com/legal/troubleshooter/1114905) pour retirer le contenu qui viole le droit d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)


### apple_store

> Si l'application malveillante est hébergée sur l'App Store, suivez [ces instructions](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=fr) pour retirer le contenu qui viole le droit d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)

### physical_sec_end

> Si vous craignez pour votre bien-être physique, veuillez contacter les organisations ci-dessous qui peuvent vous soutenir.

:[](organisations?services=physical_sec) 


### account_end

> Si vous êtes toujours victime d'usurpation d'identité ou si votre compte est toujours compromis, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account&services=legal)


### app_end

> Si la fausse application n'a pas été retirée, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account&services=legal)

### web_protection_end

> Si vos demandes de retrait n'ont pas abouti, vous pouvez essayer de contacter les organisations ci-dessous pour obtenir de l'aide supplémentaire.

:[](organisations?services=web_protection)

### secure_comms_end

> Si vous avez besoin d'aide ou de recommandations sur l'hameçonnage, la sécurité et le chiffrement des courriels et la sécurité des communications en général, vous pouvez communiquer avec ces organisations :

:[](organisations?services=secure_comms)


### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Pour éviter toute autre tentative d'usurpation d'identité, lisez les conseils ci-dessous.

### final_tips

- Créez des mots de passe solides, complexes et uniques pour tous vos comptes.
- Pensez à utiliser un gestionnaire de mots de passe pour créer et stocker les mots de passe afin de pouvoir utiliser de nombreux mots de passe différents sur différents sites et services sans avoir à les mémoriser. 
- Activez l'authentification à deux facteurs (2FA) pour vos comptes les plus importants. 2FA offre une plus grande sécurité de compte en exigeant l'utilisation de plus d'une méthode pour se connecter à vos comptes. Cela signifie que même si quelqu'un s'emparait de votre mot de passe principal, il ne pourrait pas accéder à votre compte à moins qu'il n'ait également votre téléphone portable ou un autre moyen secondaire d'authentification.
- Vérifiez vos profils sur les plateformes de réseautage social. Certaines plateformes offrent une fonction permettant de vérifier votre identité et de la relier à votre compte.
- Cartographiez votre présence en ligne. L'auto-investigation sur sa présence en ligne consiste à recueillir les informations sur soi-même présentes en ligne pour empêcher les acteurs malveillants de trouver et d'utiliser ces informations pour se faire passer pour vous.
- Configurez des alertes Google. Vous pouvez recevoir des e-mails lorsque de nouveaux résultats pour un sujet apparaissent dans Google Search. Par exemple, vous pouvez obtenir des informations sur les mentions de votre nom ou de celui de votre organisation/collectif.
- Capturez votre page Web telle qu'elle apparaît maintenant pour l'utiliser comme preuve dans le futur. Si votre site Web permet les robots d'exploration, vous pouvez utiliser la Wayback Machine, offerte par archive.org. Visitez [cette page](https://archive.org/web/) et cliquez sur le bouton "Save Page Now".


#### resources

- [Créez des mots de passe solides et uniques](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Vue d'ensemble animée : Utilisez les gestionnaires de mots de passe pour rester en sécurité en ligne](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Choisir un gestionnaire de mots de passe](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Comment utiliser KeePassXC - un gestionnaire de mots de passe open source sécurisé](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Authentification à deux facteurs (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Un guide pour prévenir la publication d'informations personnelles non consentie (doxing)](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Archivez votre site web](https://archive.org/web/)

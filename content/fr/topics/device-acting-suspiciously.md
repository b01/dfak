---
layout: topic
title: "Mon appareil agit de façon suspecte"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: fr
summary: "Si votre ordinateur ou téléphone agit de façon suspecte, il y a peut être des logiciels non désirés ou malveillants dans votre appareil."
date: 2019-03
permalink: /fr/topics/device-acting-suspiciously
parent: /fr/
---

# My device is acting suspiciously

Les attaques de logiciels malveillants ont évolué et sont devenues très sophistiquées au fil des années. Ces attaques posent des menaces multiples et différentes et peuvent avoir de graves implications pour votre infrastructure et vos données personnelles et organisationnelles.

Les attaques de logiciels malveillants se présentent sous différentes formes, telles que les virus, l'hameçonnage, les rançongiciels, les chevaux de Troie et les kits de prise de contrôle de votre appareil (rootkits). Certaines des menaces sont : le crash d'ordinateurs, le vol de données (c'est à dire : informations d'identification de compte, informations financières, identifiants de compte bancaire), un attaquant qui vous fait chanter pour lui payer une rançon en prenant le contrôle de votre appareil, ou qui l'utilise pour lancer des attaques par déni de service (DDoS).

Certaines méthodes couramment utilisées par les attaquants pour vous compromettre, vous et vos périphériques, semblent être des activités régulières, telles que :

- Un courriel ou un message sur les médias sociaux qui vous incitera à ouvrir une pièce jointe ou à cliquer sur un lien.
- Pousser les gens à télécharger et à installer des logiciels à partir d'une source non fiable.
- Pousser quelqu'un à entrer son nom d'utilisateur et son mot de passe dans un site Web qui a l'air légitime, mais qui ne l'est pas.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour déterminer si votre appareil est probablement infecté ou non.

Si vous pensez que votre ordinateur ou votre appareil mobile a commencé à agir de façon suspecte, vous devriez d'abord penser aux symptômes.

Les symptômes qui peuvent généralement être considérés comme une activité suspecte de l'appareil, mais qui souvent ne sont pas une raison suffisante pour s'inquiéter, sont les suivants :

- Bruit de clics pendant les appels téléphoniques
- Décharge inattendue de la batterie
- Surchauffe lorsque l'appareil n'est pas utilisé
- Dispositif fonctionnant lentement

Ces symptômes sont souvent perçus à tort comme des indicateurs fiables de l'activité suspecte des appareils en question. Cependant, aucun d'entre eux pris isolément n'est une raison suffisante de s'inquiéter.

Les symptômes fiables d'un appareil compromis sont généralement :

- L'appareil redémarre fréquemment tout seul
- Plantage d'applications, en particulier après une interaction avec celles-ci
- Les mises à jour du système d'exploitation et/ou les correctifs de sécurité échouent de façon répétée
- Le témoin d'activité de la webcam s'allume lorsque la webcam n'est pas utilisée
- Un ["Écran bleu de la mort"](https://fr.wikipedia.org/wiki/%C3%89cran_bleu_de_la_mort) à répétition ou panique du noyau
- Fenêtres clignotantes
- Avertissements de l'antivirus


## Workflow

### start

Compte tenu des informations fournies dans l'introduction, si vous pensez toujours que votre appareil peut être compromis, le guide suivant peut vous aider à identifier le problème.

 - [Je pense que mon téléphone mobile agit de façon suspecte](#phone-intro)
 - [Je pense que mon ordinateur mobile agit de façon suspecte](#computer-intro)
 - [Je ne pense plus que mon appareil est compromis](#device-clean)


### device-clean

> Super ! Cependant, n'oubliez pas que ces instructions vous aideront à n'effectuer qu'une analyse rapide. Si cela suffit à identifier les anomalies visibles, des logiciels espions plus sophistiqués pourraient être capables de se cacher plus efficacement.

Si vous soupçonnez toujours que votre appareil pourrait être compromis, vous voudrez peut-être :

- [chercher une aide complémentaire](#malware_end)
- [procéder directement à une réinstallation de l'appareil](#reset).


### phone-intro

> Il est important de considérer comment votre appareil a pu être compromis.
>
> Depuis combien de temps avez-vous commencé à soupçonner que votre appareil agissait de façon suspecte ?
> Vous souvenez-vous d'avoir cliqué sur des liens de sources inconnues ?
> Avez-vous reçu des messages de personnes que vous ne reconnaissez pas ?
> Avez-vous installé un logiciel non signé ?
> L'appareil a t-il été hors de votre possession ?


Réfléchissez à ces questions pour essayer d'identifier les circonstances, le cas échéant, qui ont conduit à la compromission de votre appareil.

 - [J'ai un appareil Android](#android-intro)
 - [J'ai un appareil iOS](#ios-intro)


### android-intro

> Vérifiez d'abord si des applications inconnues sont installées sur votre appareil Android.
>
> Vous trouverez la liste des applications dans la section "Applications" du menu Paramètres. Identifiez toutes les applications qui n'ont pas été préinstallées avec votre appareil et que vous ne vous souvenez pas avoir téléchargées.
>
> Si vous soupçonnez l'une des applications de la liste, lancez une recherche sur le Web et recherchez des ressources pour voir s'il existe des rapports qui identifient l'application comme malveillante.

Avez-vous trouvé des applications suspectes ?

 - [Non](#android-unsafe-settings)
 - [Oui, j'ai identifié des applications potentiellement malicieuses](#android-badend)


### android-unsafe-settings

> Android donne aux utilisateurs la possibilité d'activer l'accès à un plus bas niveau de leur appareil. Cela peut être utile pour les développeurs de logiciels, mais cela peut aussi exposer les appareils à des attaques supplémentaires. Vous devriez examiner ces paramètres de sécurité et vous assurer qu'ils sont réglés sur les options les plus sûres. Les fabricants peuvent expédier des appareils dont les paramètres par défaut ne sont pas sécurisés. Ces paramètres doivent être revus même si vous n'avez pas fait les changements vous-même.
>
> #### Applications non signées
>
> Android bloque normalement l'installation des applications qui ne sont pas chargées depuis le Google Play Store. Google a des processus pour examiner et identifier les applications malveillantes sur le Play Store. Les attaquants tentent souvent d'éviter ces contrôles en proposant des applications malveillantes directement à l'utilisateur en partageant un lien ou un fichier avec lui. Il est important de vous assurer que votre appareil ne permet pas l'installation d'applications provenant de sources non fiables.
>
> Allez dans la section "Sécurité" de vos paramètres Android et assurez-vous que l'option Installer l'application depuis une source inconnue est désactivée.
>
> #### Mode développeur et accès ADB (Android Debug Bridge)
>
> Android permet aux développeurs d'exécuter directement des commandes sur le système d'exploitation sous-jacent en "mode développeur". Lorsqu'il est activé, cela expose les appareils à des attaques physiques. Une personne ayant un accès physique à l'appareil pourrait utiliser le mode Développeur pour télécharger des copies de données privées de l'appareil ou pour installer des applications malveillantes.
>
> Si vous voyez un menu Mode développeur dans les paramètres de votre appareil, vous devez vous assurer que l'accès ADB est désactivé.
>
> #### Google Play Protect
>
> Le service Google Play Protect est disponible sur tous les appareils Android récents. Il effectue des analyses régulières de toutes les applications installées sur votre appareil. Play Protect peut également supprimer automatiquement toutes les applications malveillantes connues de votre appareil. L'activation de ce service envoie des informations à propos de votre appareil (applications installées) à Google.
>
> Google Play Protect peut être activé dans les paramètres de sécurité de votre appareil. Plus d'informations sont disponibles sur le site [Play Protect](https://www.android.com/intl/fr_fr/play-protect/).

Avez-vous identifié des réglages peu sûrs ?

- [Non](#android-bootloader)
- [Oui, j'ai identifié des réglages potentiellement peu sûrs](#android-badend)


### android-bootloader

> Le chargeur d'amorçage (ou bootloader) Android est un logiciel clé qui s'exécute dès que vous allumez votre appareil. Le chargeur d'amorçage permet au système d'exploitation de démarrer et d'utiliser le matériel. Un chargeur d'amorçage compromis donne à un attaquant un accès complet au périphérique. La majorité des fabricants livrent leurs appareils avec un chargeur d'amorçage verrouillé. Une façon courante de savoir si le chargeur d'amorçage signé du fabricant a été modifié est de redémarrer votre appareil et de rechercher le logo d'amorçage. Si un triangle jaune avec un point d'exclamation apparaît, c'est que le chargeur d'amorçage original a été remplacé. Votre appareil peut également être compromis s'il affiche un écran d'avertissement de chargeur d'amorçage déverrouillé et que vous ne l'avez pas déverrouillé vous-même pour installer une ROM Android personnalisée telle que CyanogenMod. Vous devez effectuer une réinitialisation d'usine de votre périphérique s'il affiche un écran d'avertissement de chargeur d'amorçage déverrouillé auquel vous ne vous attendez pas.

Le chargeur d'amorçage est-il compromis ou votre périphérique utilise-t-il le chargeur d'amorçage d'origine ?

- [Le chargeur d'amorçage sur mon périphérique est compromis](#android-badend)
- [Mon périphérique utilise le chargeur d'amorçage d'origine](#android-goodend)


### android-goodend

> Il ne semble pas que votre appareil ait été compromis.

Êtes-vous toujours inquiet que votre appareil soit compromis ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai résolu mes problèmes](#resolved_end)


### android-badend

> Votre appareil peut être compromis. Une [réinitialisation](#reset) d'usine supprimera probablement toute menace présente sur votre appareil. Cependant, ce n'est pas toujours la meilleure solution. De plus, vous voudrez peut-être enquêter davantage sur la question pour déterminer votre niveau d'exposition et la nature précise de l'attaque que vous avez subie.
>
> Vous pouvez utiliser un outil d'autodiagnostic appelé [VPN d'urgence](https://www.civilsphereproject.org/emergency-vpn), ou demander l'assistance d'une organisation qui peut vous aider.

Souhaitez-vous obtenir de l'aide supplémentaire ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai un réseau de soutien local auquel je peux m'adresser](#resolved_end)


### ios-intro

> Vérifiez les paramètres de l'iOS pour voir s'il y a quelque chose d'inhabituel.
>
> Dans l'application Paramètres, vérifiez que votre appareil est lié à votre Apple ID. Le premier élément dans le menu à gauche doit être votre nom ou le nom que vous utilisez pour votre Apple ID. Cliquez dessus et vérifiez que l'adresse e-mail correcte est affichée. Au bas de cette page, vous trouverez une liste avec les noms et modèles de tous les périphériques iOS liés à cet Apple ID.

 - [Toutes les informations sont correctes et je contrôle toujours mon Apple ID](#ios-goodend)
 - [Le nom ou d'autres détails sont incorrects ou je vois des périphériques dans la liste qui ne sont pas les miens](#ios-finmalmalheureuse)


> Check the iOS settings to see if there is anything unusual.
>
> In the Settings app, check that your device is tied to your Apple ID. The first menu item on the left-hand-side should be your name or the name you use for your Apple ID. Click on it and check that the correct email address is shown. At the bottom of this page you will see a list with the names and models of all iOS devices tied to this Apple ID.

 - [All the information is correct and I am still in control of my Apple ID](#ios-goodend)
 - [The name or others details are incorrect or I see devices in the list that are not mine](#ios-badend)


### ios-goodend

> Il ne semble pas que votre appareil ait été compromis.

Êtes-vous toujours inquiet que votre appareil soit compromis ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai résolu mes problèmes](#resolved_end)


### ios-badend

> Votre appareil peut être compromis. Une réinitialisation d'usine supprimera probablement toute menace présente sur votre appareil. Cependant, ce n'est pas toujours la meilleure solution. De plus, vous voudrez peut-être enquêter davantage sur la question pour déterminer votre niveau d'exposition et la nature précise de l'attaque que vous avez subie.
>
> Vous pouvez utiliser un outil d'autodiagnostic appelé [VPN d'urgence](https://www.civilsphereproject.org/emergency-vpn), ou demander l'assitance d'une organisation qui peut vous aider.

Souhaitez-vous obtenir de l'aide supplémentaire ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai un réseau de soutien local auquel je peux m'adresser](#resolved_end)


### computer-intro

> **Remarque : Si vous êtes victime d'une attaque de type rançonnage, adressez-vous directement à [ce site Web](https://www.nomoreransom.org/).**
>
> Ce déroulé va vous aidera à enquêter sur les activités suspectes de votre ordinateur. Si vous aidez une personne à distance, vous pouvez essayer de suivre les étapes décrites dans les liens ci-dessous en utilisant un logiciel de prise de contrôle du bureau à distance comme TeamViewer, ou vous pouvez consulter des outils d'investigation à distance comme [Google Rapid Response (GRR)](https://github.com/google/grr). Gardez à l'esprit que le temps de latence et la fiabilité du réseau seront essentiels pour y parvenir correctement.

Veuillez sélectionner votre système d'exploitation :

 - [J'ai un ordinateur Windows](#windows-intro)
 - [J'ai un ordinateur Mac](#mac-intro)


### windows-intro

> Vous pouvez suivre ce guide d'introduction pour enquêter sur les activités suspectes sur les périphériques Windows :
>
> [Comment faire un examen des activités suspectes sur Windows par Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Ces instructions vous ont-elles aidé à identifier une activité malveillante ?

 - [Oui, je pense que l'ordinateur est infecté](#device-infected)
 - [Non, aucune activité malveillante n'a été identifiée](#device-clean)


### mac-intro

> Pour identifier une infection potentielle sur un ordinateur Mac, vous devez suivre les étapes suivantes :
>
> 1. Vérifier si des programmes suspects démarrent automatiquement
> 2. Vérifier les processus suspects en cours d'exécution
> 3. Vérifiez les extensions suspectes du noyau
>

> Le site Web [Objective-See](https://objective-see.com) fournit plusieurs utilitaires gratuits qui facilitent ce processus :
>
> [KnockKnock](https://objective-see.com/products/knockknock.html) peut être utilisé pour identifier tous les programmes qui sont configurés pour démarrer automatiquement.
> [TaskExplorer](https://objective-see.com/products/taskexplorer.html) peut être utilisé pour vérifier les processus en cours d'exécution et identifier ceux qui semblent suspects (par exemple parce qu'ils ne sont pas signés, ou parce qu'ils sont identifiés par VirusTotal).
> [KextViewr](https://objective-see.com/products/kextviewr.html) peut être utilisé pour identifier toute extension suspecte du noyau qui est chargée sur un ordinateur Mac.
>
> Dans le cas où ceux-ci ne révèlent rien de suspect dans l'immédiat et que vous souhaitez effectuer d'autres recherches, vous pouvez utiliser [Snoopdigg](https://github.com/botherder/snoopdigg). Snoopdigg est un utilitaire qui simplifie le processus de collecte d'informations sur le système et la réalisation d'un instantané complet de la mémoire.
>
> Un outil supplémentaire qui pourrait être utile pour recueillir plus de détails (mais qui nécessite une certaine familiarité avec les commandes du terminal) est [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) de la société américaine de cybersécurité CrowdStrike.

Ces instructions vous ont-elles aidé à identifier une activité malveillante ?

 - [Oui, je pense que l'ordinateur est infecté](#device-infected)
 - [Non, aucune activité malveillante n'a été identifiée](#device-clean)

### device-infected

Oh non ! Pour vous débarrasser de l'infection que vous pouvez :

- [Demander de l'aide supplémentaire](#malware_end)
- [Procéder directement à une réinitialisation de l'appareil](#reset).

### reset

> Vous voudrez peut-être réinitialiser votre appareil comme mesure de précaution supplémentaire. Les guides ci-dessous vous fourniront les instructions appropriées pour votre type d'appareil :
>
> - [Android](https://www.howtogeek.com/248127/how-to-wipe-your-android-device-and-restore-it-to-factory-settings/)
> - [iOS](https://support.apple.com/en-us/HT201252)
> - [Windows](https://support.microsoft.com/en-us/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/en-us/HT201314)

Avez-vous besoin d'aide supplémentaire ?

- [Oui](#malware_end)
- [Non](#resolved_end)


### malware_end

Si vous avez besoin d'aide supplémentaire pour traiter un dispositif infecté, vous pouvez vous adresser aux organisations listées ci-dessous.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Nous espérons que ce guide de dépannage vous a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Derniers_conseils

Voici quelques conseils pour vous éviter d'être la proie d'un attaquant qui tente de compromettre vos appareils et vos données :

- Vérifiez toujours deux fois la légitimité de tout email que vous recevez, d'un fichier que vous avez téléchargé ou d'un lien vous demandant les détails de connexion de votre compte.
- Pour en savoir plus sur la façon de protéger votre appareil contre les infections par des logiciels malveillants, consultez les guides en lien dans les ressources.

#### Ressources

- [Security in a Box - Avoiding Malware and Phishing Attacks"](https://securityinabox.org/en/guide/malware/#avoiding-malware-and-phishing-attacks)
- [Security in a Box - Protect your device from malware and phishing attacks](https://securityinabox.org/en/guide/malware/)

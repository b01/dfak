---
layout: page
title: Email
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/email.md
parent: /fr/
published: true
---

Le contenu de votre message ainsi que le fait que vous avez communiqué avec l'organisme peuvent être accessibles par les gouvernements ou les organismes d'application de la loi.


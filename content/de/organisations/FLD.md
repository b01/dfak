---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FLD.png
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: emergency 24/7, global; regular work Monday-Friday at office hours, IST (UTC+1), staff are located in various time zones in different regions
response_time: same or next day in case of emergency
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders is an international organization based in Ireland
that works for the integrated protection of human rights defenders at
risk. Front Line Defenders provides rapid and practical support to human rights defenders at immediate risk through security grants, physical and digital security training, advocacy and campaigning.

Front Line Defenders operate an emergency support hotline 24/7 on
+353-121-00489 for human rights defenders at immediate risk in Arabic,
English, French, Russian or Spanish. When human rights defenders face an immediate threat to their lives Front Line Defenders can help with
temporary relocation. Front Line Defenders provides training in physical security and digital security. Front Line Defenders also publicises the cases of human rights defenders at risk and campaigns and advocates at the international level including the EU, UN, interregional mechanisms and with governments directly for their protection.

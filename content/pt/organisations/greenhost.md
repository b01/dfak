---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost offers IT services with an ethical and sustainable approach. Our service offerings include web hosting, cloud services, and powerful niche offerings in information security. Collaborating with cultural organizations and technical pioneers, we strive to give our users the full opportunities of the Internet while also protecting their privacy. We are actively involved in open source development, and partake in various projects in the areas of technology, journalism, culture, education, sustainability, and Internet freedom.

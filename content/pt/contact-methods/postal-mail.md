---
layout: page
title: Postal Mail
author: mfc
language: pt
summary: Contact methods
date: 2018-09
permalink: /pt/contact-methods/postal-mail.md
parent: /pt/
published: true
---

Sending mail is a slow communication method if you are facing an urgent situation. Depending on the jurisdictions where the mail travels, the authorities may open the mail, and they often track the sender, sending location, recipient, and destination location.
---
layout: page
title: "Проблема с доступом к аккаунту"
author: RaReNet
language: ru
summary: "Проблемы с доступом к email, аккаунту в соцсети или на каком-то сайте? В аккаунте происходят какие-то действия без вашего ведома? Есть много способов решить эту проблему."
date: 2019-03
permalink: /ru/topics/account-access-issues/
parent: /ru/
---


# У меня проблема с доступом к аккаунту

Гражданские активисты широко используют для связи социальные сети и прочие аккаунты коммуникаций. И не только для связи, но и для обмена знаниями и продвижения собственных интересов. Неудивительно, что эти аккаунты часто становятся мишенями. Злоумышленники вредят аккаунтам, а значит, самим активистам и тем, с кем они общаются.

В этом руководстве мы попытаемся вам помочь, если вы потеряли доступ к своему аккаунту из-за таких злоумышленников.

Вот вопросы, которые позволят лучше понять суть вашей проблемы и подыскать решения.

## Workflow

### Password_Typo

> Иногда проблема доступа к аккаунту возникает из-за ошибки в наборе пароля. Бывает, что человек забывает переключить раскладку клавиатуры или оставляет нажатой клавишу CapsLock.
>
> Попробуйте набрать логин и пароль в текстовом редакторе. Скопируйте и вставьте их из редактора в форму на сайте. Убедитесь, что раскладка клавиатуры правильная, а CapsLock выключен.

Это помогло получить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#What_Type_of_Account_or_Service)

### What_Type_of_Account_or_Service

С каким аккаунтом проблема?

- [Facebook](#Facebook)
- [Страница Facebook](#Facebook_Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service_Name) -->


### Facebook_Page

Есть ли у страницы другие администраторы?

- [Да](#Other_admins_exist)
- [Нет](#Facebook_Page_recovery_form)

### Other_admins_exist

У других администраторов та же проблема?

- [Да](#Facebook_Page_recovery_form)
- [Нет](#Other_admin_can_help)

### Other_admin_can_help

> Пожалуйста, попросите других администраторов добавить вас к списку администраторов.

Проблема решена?

- [Да](#Fb_Page_end)
- [Нет](#account_end)


### Facebook_Page_recovery_form

> Пожалуйста, зайдите в Facebook и используйте [эту форму для восстановления доступа к странице](https://www.facebook.com/help/contact/164405897002583)).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_google)
- [Нет](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Вы получили письмо о критической проблеме с безопасностью от Google?

- [Да](#Email_received_google)
- [Нет](#Recovery_Form_google)

### Email_received_google

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery_Link_Found_google)
- [Нет](#Recovery_Form_google)

### Recovery_Link_Found_google

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_google)

### Recovery_Form_google

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://support.google.com/accounts/answer/7682439?hl=ru).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_yahoo)
- [Нет](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

Вы получали письмо от Yahoo о смене пароля для вашего аккаунта?

- [Да](#Email_received_yahoo)
- [Нет](#Recovery_Form_Yahoo)

### Email_received_yahoo

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery_Link_Found_Yahoo)
- [Нет](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> Пожалуйста, воспользуйтесь [этими инструкциями для восстановления доступа к аккаунту](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_Twitter)
- [Нет](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

Вы получали письмо от Twitter о смене пароля для вашего аккаунта?

- [Да](#Email_received_Twitter)
- [Нет](#Recovery_Form_Twitter)

### Email_received_Twitter

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery_Link_Found_Twitter)
- [Нет](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://help.twitter.com/forms/restore).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### ProtonMail

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Эти советы помогли?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_Hotmail)
- [Нет](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

Вы получали письмо от Hotmail о смене пароля для аккаунта Microsoft?

- [Да](#Email_received_Hotmail)
- [Нет](#Recovery_Form_Hotmail)

### Email_received_Hotmail

В письме есть ссылка для восстановления пароля?

- [Да](#Recovery_Link_Found_Hotmail)
- [Нет](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://account.live.com/acsr).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


### Facebook

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_Facebook)
- [Нет](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

Вы получали от Facebook письмо "Смена пароля на Facebook"?

- [Да](#Email_received_Facebook)
- [Нет](#Recovery_Form_Facebook)

### Email_received_Facebook

Говорится ли в письме "Если это были не вы, узнайте, как вы можете защитить свой аккаунт" (с гиперссылкой)?

- [Да](#Recovery_Link_Found_Facebook)
- [Нет](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> Используйте ссылку "защитить свой аккаунт" для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту по этой ссылке?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://www.facebook.com/login/identify).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_Instagram)
- [Нет](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

Вы получали от Instagram письмо со словами о том, что "ваш пароль... был изменён"?

- [Да](#Email_received_Instagram)
- [Нет](#Recovery_Form_Instagram)

### Email_received_Instagram

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery_Link_Found_Instagram)
- [Нет](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> Используйте ссылку "сбросить его здесь" для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> Пожалуйста, используйте [эту форму для восстановления доступа к аккаунту](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


### Fb_Page_end

Мы очень рады, что проблема решена. Пожалуйста, прочтите эти рекомендации: они помогут вам минимизировать риск утраты доступа к вашей странице в будущем:

- Всем администраторам страницы лучше включить двухфакторную аутентификацию в настройках своих аккаунтов.
- Предоставляйте права администратора только тем людям, кому верите, с кем есть оперативная связь.

### account_end

Если все предложенные меры не помогли вам восстановить доступ к аккаунту, можете обратиться к организациям, предлагающим помощь в этой сфере:

:[](organisations?services=account)

### resolved_end

Надеемся, КЭЦП был вам полезен. Пожалуйста, потратьте немного времени на обратную связь ([email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)).

### final_tips

Пожалуйста, прочтите эти рекомендации, чтобы снизить вероятность потери доступа к аккаунтам в будущем: 

- Для всех аккаунтов, где это возможно, есть смысл включать двухфакторную аутентификацию.
- Никогда не используйте один и тот же пароль для двух и более аккаунтов. Если всё-таки использовали, смените пароли как можно скорее.
- Парольный менеджер поможет создавать и запоминать уникальные, надёжные пароли для всех ваших аккаунтов.
- Будьте осторожны, используя открытые сети wi-fi, которым нет оснований доверять. Возможно, лучше работать в них через VPN или Tor.

#### Что почитать

* [Безопасность-в-коробке. Как создавать и хранить надёжные пароли](https://securityinabox.org/ru/guide/passwords/)
* [Security Self-Defense. Самозащита в социальных сетях](https://ssd.eff.org/ru/module/%D1%81%D0%B0%D0%BC%D0%BE%D0%B7%D0%B0%D1%89%D0%B8%D1%82%D0%B0-%D0%B2-%D1%81%D0%BE%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85-%D1%81%D0%B5%D1%82%D1%8F%D1%85)





<!--- Edit the following to add another service recovery workflow:
#### service_name

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I_have_access_to_recovery_email_google)
- [Нет](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Вы получили от service_name сообщение "[Password Change Email Subject]"?

- [Да](#Email_received_service_name)
- [Нет](#Recovery_Form_service_name

### Email_received_service_name

> В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery_Link_Found_service_name)
- [Нет](#Recovery_Form_service_name)

### Recovery_Link_Found_service_name

> Пожалуйста, используйте [Recovery Link Description](URL) для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту с помощью ссылки "[Recovery Link Description]"?

- [Да](#resolved_end)
- [Нет](#Recovery_Form_service_name)

### Recovery_Form_service_name

> Пожалуйста, используйте эту форму для восстановления доступа к аккаунту: [Link to the standard recovery form].
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

-->

---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Миссия Digital Defenders Partnership (DDP) — защита и продвижение онлайновых свобод, а также борьба за свободу интернета и против растущих угроз, особенно в странах с репрессивными режимами. Мы координируем экстренную помощь для частных лиц и организаций: правозащитников, журналистов, гражданских активистов, блогеров. В центре нашего внимания — человек, личность. Наши основные ценности — прозрачность, права человека, инклюзивность и многообразие, равенство, конфиденциальность и свобода. DDP была создана в 2012 году Коалицией за свободу интернета (Freedom Online Coalition, FOC).

У DDP есть три разные программы финансирования, которые направлены на экстренные ситуации, а также долгосрочные гранты, призванные поддерживать ресурсы организаций. Кроме того, мы координируем программу Digital Integrity Fellowship, в рамках которой организации получают адресную помощь по цифровой безопасности и приватности (включая тренинги), и программу Сети быстрого реагирования (Rapid Response Network).

---
name: HURIDOCS
website: https://www.huridocs.org
logo: huridocs-signature-logo.jpg
languages: Español, English, Русский, Français, العربية, Deutsch, Nederlands, հայերէն, Polski
services: in_person_training, org_security, assessment, secure_comms, account, device_security
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7
response_time: 12-48 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.huridocs.org/contacts_2/
email: info@huridocs.org
pgp_key_fingerprint: E7E503AB
phone: 0041227555252
mail: Rue de Varembe 3, 1202 Geneva, Switzerland
initial_intake: 
---

HURIDOCS помогает правозащитникам использовать информацию и современные технологии для предания гласности нарушений прав человека и для восстановления справедливости как для жертв, так и для их преследователей.


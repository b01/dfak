---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 2 hours
contact_methods: web, email, pgp
email: help@accessnow.org
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Организация Access Now предлагает горячую линию по цифровой безопасности. Она помогает как частным лицам, так и организациям по всему миру поддерживать должный уровень онлайновой безопасности. Если вы в группе риска, мы поможем укрепить ваши практики в области цифровой безопасности и уменьшить вероятный ущерб. Если вас уже атакуют, мы готовы предложить оперативную помощь.

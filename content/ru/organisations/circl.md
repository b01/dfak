---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, French, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office_hours, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL — группа быстрого реагирования по цифровой безопасности для частного бизнеса, сообществ и НКО в Люксембурге.

CIRCL — надёжный партнёр для всякого пользователя, компании или организации из этой страны, которая оказывается под ударом. Эксперты CIRCL работают, как бригада пожарных: они быстро и эффективно реагируют на угрозы и инциденты, как произошедшие, так и в процессе подготовки.

В задачи CIRCL входят систематический и оперативный сбор данных о цифровых угрозах, анализ, информирование сообщества, реагирование.
